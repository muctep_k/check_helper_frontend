# Create image based on the official Node image from dockerhub
FROM node:16

RUN mkdir /project
WORKDIR /project

COPY . .

RUN npm i
CMD ["npm", "start"]