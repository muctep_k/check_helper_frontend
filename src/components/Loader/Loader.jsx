import React from "react";
import "./spinner.css";

export default function Loader() {
  return (
      <div className="loading-spinner"></div>
  );
}