import TokenService from "../../services/TokenService";
import {Link, useNavigate, useLocation, NavLink} from "react-router-dom";
const Header = () => {
    const isAuthorized = TokenService.getAccessToken() !== null;

    const navigate = useNavigate();
    const location = useLocation();
    console.log(location.pathname)
    console.log(location.pathname==="/")
    const HandleAuthorization = (e) => {
        e.preventDefault();
        if (isAuthorized){
            TokenService.clearToken();
            navigate('/')
        }
        else{
            navigate('/login')
        }
    }
    const handleRegistrationButton = (e) => {
        e.preventDefault();
        navigate('/register')
    }
    return (
        <header className="header-section d-lg-block d-none">
            <div className="header-top">
                <div className="container">
                    <div className="row d-flex justify-content-between align-items-center">
                        <div className="col-6">
                            <div className="header-top--left">
                                <span>Product Check Helper</span>
                            </div>
                        </div>
                        <div className="col-6">
                            <div className="header-top--right">
                                <ul className="header-user-menu">
                                    <li className="has-user-dropdown">
                                        <a href="">Русский</a>
                                        <ul className="user-sub-menu">
                                            <li><a href=""><img className="user-sub-menu-in-icon" alt=""/> Русский</a>
                                            </li>
                                            <li><a href=""><img className="user-sub-menu-in-icon" alt=""/> Кыргызча</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li><a href="" onClick={HandleAuthorization}><i className="icon-repeat"></i>{isAuthorized? 'Выйти': 'Авторизация'}</a></li>
                                    {!isAuthorized && (<li><a href="" onClick={handleRegistrationButton}><i className="icon-repeat"></i>Регистрация</a></li>)}
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className="header-bottom sticky-header">
                <div className="container">
                    <div className="row">
                        <div className="col-12">
                            <div className="main-menu">
                                <nav>
                                    <ul>
                                        <li className="has-dropdown">
                                            <NavLink to="/">Главная
                                                <i className={({ isActive }) => (isActive ? 'active' : 'inactive')}></i></NavLink>
                                        </li>
                                        <li className="has-dropdown has-megaitem">
                                            <NavLink to="/scan">Сканировать товар <i
                                                className={ location.pathname==="scan" ? "active": "" }></i></NavLink>

                                        </li>
                                        <li className="has-dropdown">
                                            <NavLink to="/recommendations">Рекомендации<i
                                                className={({ isActive }) => (isActive ? 'active' : 'inactive')}></i></NavLink>
                                        </li>
                                        <li className="has-dropdown">
                                            <NavLink to={"/menu"}>Меню<i className={location.pathname==="/menu" ? "active": "" }></i></NavLink>
                                        </li>
                                        {( isAuthorized &&
                                            <li>
                                                <NavLink className={({ isActive }) => (isActive ? 'active' : 'inactive')}
                                                      to={"/profile"}>Мой профиль</NavLink>
                                            </li>
                                        )}
                                        <li>
                                            <NavLink className={({ isActive }) => (isActive ? 'active' : 'inactive')}
                                                to={'/contacts'}
                                            >Контакты</NavLink>
                                        </li>
                                    </ul>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
    )
}
export default Header;