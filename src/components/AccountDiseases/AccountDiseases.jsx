import {useEffect, useState} from "react";
import requestService from "../../services/RequestService";
import {NotificationManager} from "react-notifications";
import TokenService from "../../services/TokenService";
import Loader from "../Loader/Loader";

const AccountDiseases = () => {
    const [tags, setTags] = useState([]);
    const [diseases, setDiseases] = useState([]);
    const [userDiseases, setUserDiseases] = useState([]);
    const [isLoading, setIsLoading] = useState(false);
    const [diseasesPage, setDiseasesPage] = useState(1);
    const [canLoadMore, setCanLoadMore] = useState(false);
    const getTags = () => {
        requestService.get('/disease_recommendations/diseasetags/')
            .then((res) => {
            let res_tags = res.data;
            for (let tag of res_tags){
                tag.selected = false;
            }
            setTags(res_tags)
        })
            .catch((res) => {
            NotificationManager.error('Ошибка запроса к серверу', 'Ошибка', 5000);
        })
    }
    const getDiseases = (page = diseasesPage) => {
        let diseasesUrl = '/disease_recommendations/diseases/?page=' + page
        let selectedTags = tags.filter((tag) => tag.selected)
        if (selectedTags && selectedTags.length !== 0 ){
            diseasesUrl += '&tags=' + selectedTags.map((tag) => tag.id).join(',');
        }

        requestService.get(diseasesUrl)
            .then((res) => {
            if (page < 2)
            setDiseases(res.data.results);
            else
                setDiseases((prevState) => prevState.concat( res.data.results))
            if (res.data.next){
                setCanLoadMore(true);
                setDiseasesPage(page + 1);
            }
            else{
                setCanLoadMore(false);
            }

        })
            .catch((res) => {
            NotificationManager.error('Ошибка запроса к серверу', 'Ошибка', 5000);
        })
    }
    const getUserDiseases = () => {
        requestService.get('/accounts/users/' + TokenService.getUserId() + '/diseases/')
            .then((res) => {
            setUserDiseases(res.data);
        })
            .catch((res) => {
            NotificationManager.error('Ошибка запроса к серверу', 'Ошибка', 5000);
        })
    }
    const updateUserDiseases = (diseaseIds) => {
        requestService.patch('/accounts/users/' + TokenService.getUserId() + '/diseases/', {'ids': diseaseIds} )
            .then((res) => {
            NotificationManager.success('Изменения успешно сохранены', 'Успешно', 5000);
        })
            .catch((res) => {
            NotificationManager.error('Ошибка запроса к серверу', 'Ошибка', 5000);
        })
    }
    useEffect(() => {
    getTags();
    getDiseases();
    getUserDiseases();
  }, []);

    const handleTagClicked = (e,index) => {
        e.preventDefault();
        const newTags = [...tags];
        newTags[index].selected = !newTags[index].selected;
        setTags(newTags);
        getDiseases(1);
    }
    const handleDiseaseClicked = (e, diseaseId) => {
        let newUserDiseases = [...userDiseases];
        if (e.target.checked){
            newUserDiseases.push(diseaseId);
        }
        else{
            newUserDiseases = newUserDiseases.filter((item) => item !== diseaseId)
        }
        setUserDiseases(newUserDiseases);

    }
    const handleSubmitDiseases = (e) => {
        e.preventDefault();
        updateUserDiseases(userDiseases);
    }
    const handleLoadMore = (e) => {
        e.preventDefault();
        getDiseases();
    }
    console.log(diseasesPage)
  return (
      <div>
           <h3 className={"text-center my-5" }>Ваши болезни</h3>

          <h4>Теги болезней</h4>
          <p>Теги удобны для фильтрации болезней которые расположены в нижнем блоке.</p>
            <div className="tag-link tags">
                {tags.map((tag, index)=> {
                    return <a className={tag.selected ? "active" : ""}
                              onClick={(e)=> handleTagClicked(e, index)}>
                        {tag.name}
                    </a>
                })}
            </div>

          <div className={"account_login_form"}>
              <h5 className={"mt-5"}>Болезни</h5>
                <p>Выберите определенные болезни которые были оффициально диагностированы Вашим врачом.</p>
              <form action="#">

                  <div className="sidebar-content mt-5">
                      <div className="filter-type-select row">
                                  {diseases.map((disease, index) => {
                                      return <div key={disease.id} className={"col-6"}><label className="checkbox-default" htmlFor={"disease" +  disease.id}>
                                              <input type="checkbox" id={"disease" +  disease.id}
                                                     checked={userDiseases.includes(disease.id)}
                                                     onChange={(e) => handleDiseaseClicked(e, disease.id)}

                                                    />
                                              <span>{disease.name}</span>
                                                </label>
                                          </div>
                                      })
                                  }
                      </div>
                  </div>
                  {canLoadMore &&
                      <div className={"save_button primary_btn default_button mt-5" } >
                        <button  onClick={handleLoadMore} disabled={isLoading} >
                            Загрузить ещё
                            {isLoading && <Loader/>}
                        </button>
                    </div>
                  }
                  <div className="save_button primary_btn default_button mt-5">
                        <button className="mx-auto" onClick={handleSubmitDiseases} disabled={isLoading} >
                            Сохранить
                            {isLoading && <Loader/>}
                        </button>
                  </div>
              </form>
          </div>
      </div>
  )
}
export default AccountDiseases