import Loader from "../Loader/Loader";
import {useState} from "react";
import requestService from "../../services/RequestService";
import {NotificationManager} from "react-notifications";
import TokenService from "../../services/TokenService";

const AccountDetails = () => {
    const [isLoading, setIsLoading] = useState(false);
    const [data, setData] = useState({});
    const [oldData, setOldData] = useState({});
    const API_URL = 'accounts/users/' + TokenService.getUserId() + '/';
    const [error, setError] = useState({
    });
    const getData = () => {
        requestService.get(API_URL)
            .then((res) => {
            setData(res.data);
            setOldData(res.data);
        })
            .catch((res) => {
            NotificationManager.error('Ошибка запроса к серверу', 'Ошибка', 5000);
        })
    }
    if (data === {} || !data || Object.keys(data).length === 0)
        getData();
    const handleSubmit = (e) => {
        e.preventDefault();
        let to_update = {}
        for (const field in data){
            if (data[field] !== oldData[field]){
                to_update[field] = data[field];
            }
        }
        requestService.patch(
            API_URL,
            to_update)
            .then((response) => {
                if (response.status === 200){
                    NotificationManager.success('Изменения успешно сохранены', 'Успешно', 5000);
                }
                setIsLoading(false);
            })
            .catch((res)=>{
                    NotificationManager.error('Ошибка формы', 'Ошибка', 5000);
                    if (res.response.data.hasOwnProperty('non_field_errors'))
                    setError((prevState => {return {...prevState, non_field_errors: res.response.data.non_field_errors}}))
                for (let field in data){
                    if (field in res.response.data){
                        setError((prevState => {return {...prevState, [field]: res.response.data[field][0]}}))
                    }
                }
                setIsLoading(false);
            })
    }
  return (
      <div>
      <h3>Персональные данные </h3>
        <div className="login">
            <div className="login_form_container">

                <div className="account_login_form">
                    <form action="#">
                        <div className="default-form-box mb-20">
                            <label>Имя</label>
                            <input type="text" name="firstname"
                                   className={error.hasOwnProperty("firstname") ? 'is-invalid': ''}
                                   value={data.firstname}
                                    onChange={ e => {setData(prevState => {
                                return {...prevState, firstname : e.target.value}})
                                        }}/>
                            {       error.hasOwnProperty("firstname") &&
                                        <div className="invalid-error">
                                            {error.firstname}
                                        </div>
                                        }
                        </div>
                        <div className="default-form-box mb-20">
                            <label>Фамилия </label>
                            <input type="text" name="lastname"
                                   className={error.hasOwnProperty("lastname") ? 'is-invalid': ''}
                                   onChange={ e => {setData(prevState => {
                                return {...prevState, lastname : e.target.value}})
                                        }}
                                   value={data.lastname}/>
                            {       error.hasOwnProperty("lastname") &&
                                        <div className="invalid-error">
                                            {error.lastname}
                                        </div>
                                        }
                        </div>
                        <div className="default-form-box mb-20">
                            <label>Почта</label>
                            <input type="text" name="email"
                                    className={error.hasOwnProperty("email") ? 'is-invalid': ''}
                                    onChange={ e => {setData(prevState => {
                                return {...prevState, email : e.target.value}})
                                        }}
                                   value={data.email}/>
                            {       error.hasOwnProperty("email") &&
                                        <div className="invalid-error">
                                            {error.email}
                                        </div>
                                        }
                        </div>
                        <div className="default-form-box mb-20">
                            <label>Телефонный номер</label>
                            <input type="text" name="phone_number"
                            className={error.hasOwnProperty("phone_number") ? 'is-invalid': ''}
                                   onChange={ e => {setData(prevState => {
                                return {...prevState, phone_number : e.target.value}})
                                        }}
                                   value={data.phone_number}/>
                            {       error.hasOwnProperty("phone_number") &&
                                        <div className="invalid-error">
                                            {error.phone_number}
                                        </div>
                                        }
                        </div>

                        <div className="save_button primary_btn default_button">
                        <button className="mx-auto" onClick={handleSubmit} disabled={isLoading} >
                            Сохранить
                            {isLoading && <Loader/>}
                        </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
      </div>
  )
}
export default AccountDetails;