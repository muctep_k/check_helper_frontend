const Footer = () => {
    return (
    <footer className="footer-section section-top-gap-100">
        <div className="footer-top section-inner-bg">
            <div className="container">
                <div className="row ">
                    <div className="col-lg-3 col-md-3 col-sm-5">
                        <div className="footer-widget footer-widget-contact">
                            <div className="footer-logo">
                                <a href="index.html"><img src="assets/images/logo/logo.png" alt="" className="img-fluid"/></a>
                            </div>
                            <div className="footer-contact">
                                <p>Наша команда готова всегда посоветовать вам нужные и необходимые продукты для преодоления недугов.</p>
                                <div className="customer-support">
                                    <div className="customer-support-icon">
                                        <img src="assets/images/icon/support-icon.png" alt=""/>
                                    </div>
                                    <div className="customer-support-text">
                                        <span>Служба поддержки</span>
                                        <a className="customer-support-text-phone" href="tel:+996557447448">(+996) 557 447 448</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-lg-5 col-md-5 col-sm-7">
                        <div className="footer-widget footer-widget-subscribe">
                            <h6 >Подпишитесь на нашу рассылку, чтобы быть осведомленным по вашему меню</h6>
                            <form action="#" method="post">
                                <div className="footer-subscribe-box default-search-style d-flex mt-5">
                                    <input className="default-search-style-input-box border-around border-right-none subscribe-form" type="email" placeholder="Введите вашу почту" required/>
                                    <button className="default-search-style-input-btn" type="submit">Подписаться</button>
                                </div>
                            </form>
                            <p className="footer-widget-subscribe-note">Эта почта будет использована <br/> только нашей компанией.</p>

                        </div>
                    </div>
                    <div className="col-lg-4 col-md-4 col-sm-6">
                        <div className="footer-widget footer-widget-menu">
                            <h3 className="footer-widget-title">Наши соцсети</h3>
                            <p className="footer-widget-subscribe-note">Мы в соцсетях! <br/>
                                Следите за нашими новостями и будьте в курсе событий всех обновлений</p>
                            <div className="footer-menu">
                                <ul className="footer-social">
                                <li><a href="" className="facebook"><i className="fa fa-facebook"></i></a></li>
                                <li><a href="" className="twitter"><i className="fa fa-twitter"></i></a></li>
                                <li><a href="" className="youtube"><i className="fa fa-youtube"></i></a></li>
                                <li><a href="" className="pinterest"><i className="fa fa-pinterest"></i></a></li>
                                <li><a href="" className="instagram"><i className="fa fa-instagram"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div className="footer-bottom">
            <div className="container">
                <div className="row align-items-center">
                    <div className="col-lg-6 col-md-6">
                        <div className="copyright-area">
                            <p className="copyright-area-text">Все права защищены  © 2022 <a className="copyright-link" href="https://hasthemes.com/">Product check Helper</a></p>
                        </div>
                    </div>
                    <div className="col-lg-6 col-md-6">
                        <div className="footer-payment">
                            <a href=""><img className="img-fluid" src="assets/images/icon/payment-icon.png" alt=""/></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    )
}
export default Footer;