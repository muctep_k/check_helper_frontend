import {useState} from "react";
import axios from "axios";
import {useNavigate} from "react-router-dom";
import Loader from "../../components/Loader/Loader";
import {NotificationManager} from 'react-notifications';

const Registration = () => {
    const [data, setData] = useState({
        firstname: '',
        lastname: '',
        email: '',
        password: '',
        confirmPassword: ''
    });
    const navigate = useNavigate()
    const [error, setError] = useState({
    });
    const [isLoading, setIsLoading] = useState(false);
    const handleRegister = (e) => {

        e.preventDefault();

        if (data.password !== data.confirmPassword){
            setError({confirmPassword: "Пароли не совпадают"})
             NotificationManager.error('Ошибка заполнения формы', 'Ошибка', 5000);

            return;
        }
        setError({});

        axios.post('http://localhost:8000/api/v1/accounts/users/',
            data
        ).then((res) => {
            if (res.status === 201) {
                 NotificationManager.success('Вы успешно зарегистрировались. ' +
                     'Можете ввести свои данные и войти в систему', 'Успешно', 5000);
                 setTimeout(() => {navigate('/login')}, 1500);
            }
            setIsLoading(false)
        }).catch((res) => {
                if (res.response.data.hasOwnProperty('non_field_errors'))
                    setError((prevState => {return {...prevState, non_field_errors: res.response.data.non_field_errors}}))
                for (let field in data){
                    if (field in res.response.data){
                        setError((prevState => {return {...prevState, [field]: res.response.data[field][0]}}))
                    }
                }
                NotificationManager.error('Ошибка заполнения формы', 'Ошибка', 5000);
                setIsLoading(false);


        })

    }


  return (
      <div>
          <div className="breadcrumb-section">
              <div className="breadcrumb-wrapper">
                  <div className="container">
                      <div className="row">
                          <div
                              className="col-12 d-flex justify-content-between justify-content-md-between  align-items-center flex-md-row flex-column">
                              <h3 className="breadcrumb-title">Регистрация</h3>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
          <div className="customer_login">
              <div className="container">
                  <div className="row">
                      <form action="">
                        <div className="col-lg-6 col-md-6 mx-auto">
                            <div className="account_form register">
                                <h3>Регистрация</h3>
                                <div className="default-form-box mb-20">
                                        <label> Имя </label>
                                        <input type="email"
                                               className={error.hasOwnProperty("firstname") ? 'is-invalid': ''}
                                               onChange={ e => {
                                            setData(prevState =>
                                            {return {
                                                ...prevState, firstname : e.target.value}
                                            })

                                        }}/>
                                        {
                                        error.hasOwnProperty("firstname") &&
                                        <div className="invalid-error">
                                            {error.firstname}
                                        </div>
                                        }
                                    </div>
                                <div className="default-form-box mb-20">
                                        <label>Фамилия </label>
                                        <input type="email"
                                               className={error.hasOwnProperty("lastname") ? 'is-invalid': ''}
                                               onChange={ e => {
                                            setData(prevState =>
                                            {return {
                                                ...prevState, lastname : e.target.value}
                                            })

                                        }}/>
                                        {
                                        error.hasOwnProperty("lastname") &&
                                        <div className="invalid-error">
                                            {error.lastname}
                                        </div>
                                        }
                                    </div>
                                    <div className="default-form-box mb-20">
                                        <label>Почта <span>*</span></label>
                                        <input type="email"
                                               className={error.hasOwnProperty("email") ? 'is-invalid': ''}
                                               onChange={ e => {
                                            setData(prevState =>
                                            {return {
                                                ...prevState, email : e.target.value}
                                            })

                                        }}/>
                                        {
                                        error.hasOwnProperty("email") &&
                                        <div className="invalid-error">
                                            {error.email}
                                        </div>
                                        }
                                    </div>
                                    <div className="default-form-box mb-20">
                                        <label>Пароль <span>*</span></label>
                                        <input type="password"
                                               className={error.hasOwnProperty("password") ? 'is-invalid': ''}
                                               onChange={ e => {
                                            setData(prevState =>  {return{ ...prevState, password : e.target.value}})
                                        }}/>
                                        {
                                        error.hasOwnProperty("password") &&
                                        <div className="invalid-error">
                                            {error.password}
                                        </div>
                                        }
                                    </div>

                                <div className="default-form-box mb-20">
                                    <label>Подтвердите пароль <span>*</span></label>

                                    <input type="password"
                                           className={error.hasOwnProperty("confirmPassword") ? 'is-invalid': ''}
                                           onChange={ e => { setData(prevState =>
                                           {return{ ...prevState, confirmPassword : e.target.value}})
                                        }}/>
                                    {
                                        error.hasOwnProperty("confirmPassword") &&
                                        <div className="invalid-error">
                                            {error.confirmPassword}
                                        </div>
                                    }
                                </div>
                                {error.hasOwnProperty('non_field_errors') &&
                                    <div className="default-form-box mb-20 error_form">
                                        {error.non_field_errors.map((error_msg,i ) => <p style={{color: 'red'}} key={i}>{error_msg}</p>)}
                                    </div>
                                }
                                    <div className="login_submit">
                                        <button type="submit" className={'mx-auto col'} onClick={handleRegister}>Подтвердить</button>
                                        {isLoading && <Loader/>}
                                    </div>
                            </div>
                        </div>
                      </form>
                  </div>
              </div>
          </div>
      </div>
  )
}
export default Registration;