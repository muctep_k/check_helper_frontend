import {useState} from "react";
import AccountDetails from "../../components/AccountDetails/AccountDetails";
import AccountDiseases from "../../components/AccountDiseases/AccountDiseases";

const Profile = () => {
    const [tab, setTab] = useState('account-details');
    const handleTab = (e, tab) => {
        console.log(tab)
        e.preventDefault();
        setTab(tab);
    }
    return(
        <div>
            <div className="breadcrumb-section">
                <div className="breadcrumb-wrapper">
                    <div className="container">
                        <div className="row">
                            <div
                                className="col-12 d-flex justify-content-between justify-content-md-between  align-items-center flex-md-row flex-column">
                                <h3 className="breadcrumb-title">Мой профиль</h3>
                                <div className="breadcrumb-nav">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <div className="account_dashboard">
            <div className="container">
                <div className="row">
                    <div className="col-sm-12 col-md-3 col-lg-3">
                        <div className="dashboard_tab_button">
                            <ul role="tablist" className="nav flex-column dashboard-list">
                                <li><a onClick={(e) => {handleTab(e, 'account-details');}}
                                       href="#account-details" data-toggle="tab"
                                       className={"nav-link " + (tab==="account-details" && "active")} >Мои данные</a></li>
                                <li> <a onClick={(e) => {handleTab(e, 'diseases');}}
                                        href="#diseases" data-toggle="tab"
                                        className={"nav-link " +  (tab==="diseases" && "active")}>Мои болезни</a></li>
                            </ul>
                        </div>
                    </div>
                    <div className="col-sm-12 col-md-9 col-lg-9">
                        <div className="tab-content dashboard_content">
                            <div className={"tab-pane fade " + (tab === "diseases" && "show active")} >
                                <AccountDiseases/>
                            </div>
                            <div className={"tab-pane fade " + (tab === "account-details" && "show active")}>
                                <AccountDetails/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
    )
}

export default Profile;