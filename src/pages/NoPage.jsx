import {Link} from "react-router-dom";

const NoPage = () => {
    return <div className="error-section">
            <div className="container">
                <div className="row">
                    <div className="error_form">
                        <h1>404</h1>
                        <h4>Упс! Страница не найдена</h4>
                        <p>Страница на которую вы обратились - не существует, возможно она была удалена, переместена
                            или временно недоступна. Приносим свои извинения</p>
                        <div className="row">
                            <div className="col-10 offset-1 col-md-6 offset-md-3">
                                <Link to="/">Вернуться на главную</Link>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
}
export default NoPage;