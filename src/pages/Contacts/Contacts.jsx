const Contacts = () => {
  return(
  <div>
      <div className="breadcrumb-section">
        <div className="breadcrumb-wrapper">
          <div className="container">
            <div className="row">
              <div
                  className="col-12 d-flex justify-content-between justify-content-md-between  align-items-center flex-md-row flex-column">
                <h3 className="breadcrumb-title">Наши контакты</h3>
              </div>
            </div>
          </div>
        </div>
      </div>
    <div className="map-section">
        <div className="container">
            <div className="row">
                <div className="col-12">
                    <div className="mapouter">
                        <div className="gmap_canvas">
                            <iframe id="gmap_canvas"
                                    src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d2925.869271092647!2d74.57401893758524!3d42.833375199929996!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2sru!4v1654098659485!5m2!1sen!2sru"></iframe>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div className="contact-section">
      <div className="container">
          <div className="row">
              <div className="col-lg-4">
                  <div className="contact-details-wrapper section-top-gap-100">
                      <div className="contact-details">
                          <div className="contact-details-single-item">
                              <div className="contact-details-icon">
                                  <i className="fa fa-phone"></i>
                              </div>
                              <div className="contact-details-content contact-phone">
                                  <a href="tel:+012345678102">(+996) 557 447 448</a>
                                  <a href="tel:+012345678102">(+996) 700 447 448</a>
                              </div>
                          </div>
                          <div className="contact-details-single-item">
                              <div className="contact-details-icon">
                                  <i className="fa fa-globe"></i>
                              </div>
                              <div className="contact-details-content contact-phone">
                                  <a href="mailto:urname@email.com">temirlan.adma@gmail.com</a>
                                  <a href="https://manas.edu.kg/en">Manas University</a>
                              </div>
                          </div>
                          <div className="contact-details-single-item">
                              <div className="contact-details-icon">
                                  <i className="fa fa-map-marker"></i>
                              </div>
                              <div className="contact-details-content contact-phone">
                                  <span>Микрорайон Джал-23, 34</span>
                                  <span>город Бишкек</span>
                              </div>
                          </div>
                      </div>
                      <div className="contact-social">
                          <h4>Наши соцсети</h4>
                          <ul>
                              <li><a href=""><i className="fa fa-facebook"></i></a></li>
                              <li><a href=""><i className="fa fa-twitter"></i></a></li>
                              <li><a href=""><i className="fa fa-youtube"></i></a></li>
                              <li><a href=""><i className="fa fa-google-plus"></i></a></li>
                              <li><a href=""><i className="fa fa-instagram"></i></a></li>
                          </ul>
                      </div>
                  </div>
              </div>
              <div className="col-lg-8">
                  <div className="contact-form section-top-gap-100">
                      <h3>Задайте вопрос</h3>
                      <form action="https://htmlmail.hasthemes.com/jaber/mail/contact.php" method="POST">
                          <div className="row">
                              <div className="col-lg-6">
                                  <div className="default-form-box mb-20">
                                      <label htmlFor="contact-name">Имя</label>
                                      <input type="text" id="contact-name" required/>
                                  </div>
                              </div>
                              <div className="col-lg-6">
                                  <div className="default-form-box mb-20">
                                      <label htmlFor="contact-email">Почта</label>
                                      <input type="email" id="contact-email" required/>
                                  </div>
                              </div>
                              <div className="col-lg-12">
                                  <div className="default-form-box mb-20">
                                      <label htmlFor="contact-subject">Тема</label>
                                      <input type="text" id="contact-subject" required/>
                                  </div>
                              </div>
                              <div className="col-lg-12">
                                  <div className="default-form-box mb-20">
                                      <label htmlFor="contact-message">Ваш вопрос</label>
                                      <textarea id="contact-message" cols="30" rows="10"></textarea>
                                  </div>
                              </div>
                              <div className="col-lg-12">
                                  <button className="contact-submit-btn" type="submit">Отправить</button>
                              </div>
                          </div>
                      </form>
                  </div>
              </div>
          </div>
      </div>
  </div>
  </div>
)
}
export default Contacts;