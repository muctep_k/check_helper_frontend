import {useState} from "react";
import axios from "axios";
import requestService from "../../services/RequestService";
import TokenService from "../../services/TokenService";
import Loader from "../../components/Loader/Loader";
import {useNavigate} from "react-router-dom";
import {NotificationManager} from "react-notifications";

const Home = () => {
    const [email, setEmail] = useState();
    const [password, setPassword] = useState();
    const [error, setError] = useState({})
    const [isLoading, setIsLoading] = useState(false);
    const navigate = useNavigate();
    const handleSubmit = (e) => {
        setIsLoading(true);
        e.preventDefault();
        setError({});
        axios.post('http://localhost:8000/api/v1/accounts/token/',
            {email: email,
                  password: password
            }
    ).then((res) => {
        if (res.status === 200) {
          TokenService.setToken(res.data);
          requestService.defaults.headers.common.Authorization = `Bearer ${TokenService.getAccessToken()}`;
          NotificationManager.success('Вы успешно авторизовались', 'Успешно', 5000);
            setTimeout(() => {navigate('/')}, 1500);

        }
        setIsLoading(false)
    }).catch((res) => {
            for (let field in res.response.data){
                let error_msg = Array.isArray(res.response.data[field])? res.response.data[field][0] : res.response.data[field]
                setError((prevState => {return {...prevState, [field]: error_msg}}))
                }
            NotificationManager.error('Ошибка заполнения формы', 'Ошибка', 5000);

            setIsLoading(false)

        });
    }
  return (
<div>
    <div className="breadcrumb-section">
        <div className="breadcrumb-wrapper">
            <div className="container">
                <div className="row">
                    <div className="col-12 d-flex justify-content-between justify-content-md-between  align-items-center flex-md-row flex-column">
                        <h3 className="breadcrumb-title">Авторизация</h3>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div className="customer_login">
        <div className="container">
            <div className="row">
                <div className="col-lg-6 col-md-6 mx-auto">
                    <div className="account_form">
                        <h3>Авторизация</h3>
                        <form>
                            <div className="default-form-box mb-20">
                                <label>Почта <span>*</span></label>
                                <input type="email" className={error.hasOwnProperty("email") ? 'is-invalid': ''}
                                       onChange={e => setEmail(e.target.value)}/>
                                {
                                    error.hasOwnProperty("email") &&
                                    <div className="invalid-error">
                                            {error.email}
                                        </div>
                                }
                            </div>
                            <div className="default-form-box mb-20">
                                <label>Пароль <span>*</span></label>
                                <input type="password" className={error.hasOwnProperty("password") ? 'is-invalid': ''}
                                       onChange={e => setPassword(e.target.value)}/>
                                {
                                    error.hasOwnProperty("password") &&
                                    <div className="invalid-error">
                                            {error.password}
                                        </div>
                                }
                            </div>
                            {error.hasOwnProperty('detail') &&<div className="default-form-box mb-20 error_form">
                                <p style={{color: 'red'}}>{error.detail}</p>
                            </div>
                            }
                            <div className="login_submit">
                                    <button className="mb-20" onClick={handleSubmit} disabled={isLoading} >Авторизация

                                    {isLoading && <Loader/>}
                                </button>
                                <br/>
                            </div>
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
  )
}
export default Home