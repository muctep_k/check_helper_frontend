import React from "react";
import {Outlet} from "react-router-dom";
import Header from "../../components/Header";
import Footer from "../../components/Footer";
import {NotificationContainer} from "react-notifications";

const Layout = () => {
  return (
    <>
      <Header/>
     <NotificationContainer/>

      <Outlet />
      <Footer/>
    </>
  );
};

export default Layout;