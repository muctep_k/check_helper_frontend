import {useEffect, useRef, useState} from "react";
import requestService from "../../services/RequestService";
import {NotificationManager} from "react-notifications";
import TokenService from "../../services/TokenService";
import Scanner from "../../components/Scanner/Scanner";
const Scan = () => {
    const [products, setProducts] = useState([]);
    const [diseases, setDiseases] = useState([]);
    const [userDiseases, setUserDiseases] = useState([]);
    const [searchText, setSearchText] = useState('');
    const { startQrCode, decodedQRData } = Scanner({
  qrcodeMountNodeID: "reader", closeAfterScan: false
});
    const getDiseases = () => {
        let diseasesUrl = '/disease_recommendations/diseases/?page_size=100'
        diseasesUrl += '&q=' + searchText;
        requestService.get(diseasesUrl)
            .then((res) => {
            setDiseases(res.data.results);

        })
            .catch((res) => {
            NotificationManager.error('Ошибка запроса к серверу', 'Ошибка', 5000);
        })
    }
    const getUserDiseases = () => {
        if (!TokenService.getAccessTokenValidity()){
            setUserDiseases([])
            return
        }
        requestService.get('/accounts/users/' + TokenService.getUserId() + '/diseases/')
            .then((res) => {
            setUserDiseases(res.data);
        })
            .catch((res) => {
            NotificationManager.error('Ошибка запроса к серверу', 'Ошибка', 5000);
        })
    }
    const checkCode = () => {
        const code = decodedQRData.data
        console.log(userDiseases)
        let diseases_ids = userDiseases.join(',')
        requestService.get('/disease_recommendations/recommendations/' + code + '?disease_ids=' + diseases_ids)
            .then((res) => {
                setProducts((prevState) => {
                    prevState = prevState.filter((product) => product.uuid !== res.data.uuid)
                    return [...prevState, res.data]
                })
        })
            .catch((res) => {
            NotificationManager.error('Данного кода нет в базе', 'Ошибка', 5000);
        })
    }
       const handleDiseaseClicked = (e, diseaseId) => {
        let newUserDiseases = [...userDiseases];
        if (e.target.checked){
            newUserDiseases.push(diseaseId);
        }
        else{
            newUserDiseases = newUserDiseases.filter((item) => item !== diseaseId)
        }
        setUserDiseases(newUserDiseases);

    }

  useEffect(() => {
    getDiseases();
    getUserDiseases();
    startQrCode();
  }, []);

  useEffect(() => {
      getDiseases();
  }, [searchText])
    useEffect(() => {
        if (decodedQRData.isScanSuccess){
            checkCode();
        }
    }, [decodedQRData])
  return (
  <div>
    <div class="breadcrumb-section">
        <div class="breadcrumb-wrapper">
            <div class="container">
                <div class="row">
                    <div class="col-12 d-flex justify-content-between justify-content-md-between  align-items-center flex-md-row flex-column">
                        <h3 class="breadcrumb-title">Сканирование продуктов</h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="blog-section">
        <div class="container">
            <div class="row flex-column-reverse flex-lg-row">
                <div class="col-lg-3">
                    <div class="siderbar-section">

                        <div class="sidebar-single-widget">
                            <h6 class="sidebar-title">Фильтрация</h6>
                            <div class="sidebar-content">
                                <div class="search-bar">
                                    <div class="default-search-style d-flex">
                                        <input class="default-search-style-input-box border-around border-right-none"
                                               type="text"
                                               placeholder="Название болезни..."

                                               onChange={(e) => {setSearchText(e.target.value)}}/>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="sidebar-single-widget">
                            <h6 class="sidebar-title">Болезни</h6>
                            <p>Имейте ввиду, если вы не укажете болезни, то вернутся все рекомендации по продукту</p>
                            <div className="filter-type-select row mt-3">
                                  {diseases.map((disease, index) => {
                                      return <div key={disease.id}><label className="checkbox-default" htmlFor={"disease" +  disease.id}>
                                              <input type="checkbox" id={"disease" +  disease.id}
                                                     checked={userDiseases.includes(disease.id)}
                                                     onChange={(e) => handleDiseaseClicked(e, disease.id)}

                                                    />
                                              <span>{disease.name}</span>
                                                </label>
                                          </div>
                                      })
                                  }
                            </div>
                        </div>

                    </div>
                </div>
                <div class="col-lg-9">
                    <div className="row mb-5">
                        <div className="col-md-6 col-12" id="reader" style={{width: "400px", height: "300px"}}>
                        </div>
                        <div className="col-md-6 col-12">
                            <h5 style={{textAlign: "center"}}>Что означают результаты?</h5>
                            <div className="px-5 result_explain">
                                <p>После сканирования продукта возвращаются следующие результаты:</p>
                                <ul className="list-group">
                                    <ol className="list-group-item">Название продукта</ol>
                                    <ol className="list-group-item">Категория продукта</ol>
                                    <ol className="list-group-item">Цена продукта</ol>
                                    <ol className="list-group-item">Окончательный вердикт, который зависит он самого высокого процента противопоказания</ol>
                                    <ol className="list-group-item">Результаты по болезням и процентное соотношение противопоказания</ol>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="blog-grid-wrapper">
                        <h5>Количество найденных товаров: {products.length}</h5>
                        <div class="row">
                            {products.map((product) => {
                                return (
                                    <div className="col-xl-4 col-sm-6 col-12">
                                        <div className="product-default-single border-around">
                                            <div className="product-img-warp">
                                                <a href="product-details-default.html"
                                                   className="product-default-img-link">
                                                    <img src={product.photo_link}
                                                         alt="" className="product-default-img img-fluid"/>
                                                </a>
                                            </div>
                                            <div className="product-default-content">
                                                <h6 className="product-default-link"><a
                                                    href="product-details-default.html">{product.name}</a></h6>
                                                <h6 className="product-default-link"><a
                                                    href="product-details-default.html">{product.category}</a></h6>
                                                <span className="product-default-price">{product.price} 	сом</span>

                                            </div>
                                            <div className="mt-5 px-0 py-0">
                                                <span style={{color: product.verdict.code}}>{product.verdict.text}</span>

                                                {product.recommendations.map((recommendation) => {
                                                    return (
                                                        <div className="row my-5">
                                                            <div className="col-6">{recommendation.disease}</div>
                                                            <div className="col-6" style={{color: recommendation.code}}>{recommendation.percent}%</div>
                                                        </div>
                                                    )
                                                })}

                                            </div>
                                        </div>

                                    </div>
                                )
                            })}
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
  </div>
  );
}
export default Scan;