import './App.css';
import {BrowserRouter, Routes, Route} from "react-router-dom";
import Layout from "./pages/Layout/Layout";
import Home from "./pages/Home/Home";
import Login from "./pages/Login/Login"
import "./public/css/style.css"
import Registration from "./pages/Registration/Registration";
import 'react-notifications/lib/notifications.css';
import Profile from "./pages/Profile/Profile";
import NoPage from "./pages/NoPage";
import Contacts from "./pages/Contacts/Contacts";
import Scan from "./pages/Scan/Scan";

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<Layout />}>
          <Route index element={<Home />} />
          <Route path="/login" element={<Login />} />
          <Route path="/register" element={<Registration />} />
          <Route path="/profile" element={<Profile />} />
          <Route path="/contacts" element={<Contacts/>}/>
          <Route path="/scan" element={<Scan/>}/>
          <Route path="*" element={<NoPage />} />
        </Route>
      </Routes>
    </BrowserRouter>
  );
}

export default App;
